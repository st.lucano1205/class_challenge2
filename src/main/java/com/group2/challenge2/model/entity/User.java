package com.group2.challenge2.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    String typeID;
    String numberID;
    String firstName;
    String secondName;
    String lastName;
    String secondLastName;
    String numberPhone;
    String address;
    String residentCity;
}

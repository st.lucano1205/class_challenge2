package com.group2.challenge2.model.entity;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class ResponseMessage implements Serializable {
    private String statusCode;
    private String message;
    private Object user;
}

package com.group2.challenge2.controller;

import com.group2.challenge2.model.entity.ResponseMessage;
import com.group2.challenge2.model.entity.User;
import com.group2.challenge2.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class UserController {

    @Autowired
    private IUserService userService;

    @GetMapping("user")
    public ResponseEntity<?> findByIdentityInfo(String typeID, String numberID) {
        User userGet = userService.findByIdentityInfo(typeID, numberID);

        if (typeID == null || typeID.isEmpty() || numberID == null || numberID.isEmpty())
            return new ResponseEntity<>(ResponseMessage.builder()
                    .statusCode("01")
                    .message("typeID and numberID are required.")
                    .user(null)
                    .build(),
                    HttpStatus.BAD_REQUEST);

        if (userGet != null) {
            return new ResponseEntity<>(ResponseMessage.builder()
                    .statusCode("00")
                    .message("")
                    .user(userGet)
                    .build(),
                    HttpStatus.OK);
        }

        return new ResponseEntity<>(ResponseMessage.builder()
                .statusCode("02")
                .message("User not found.")
                .user(null)
                .build(),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> errorServer() {
        return new ResponseEntity<>(ResponseMessage.builder()
                .statusCode("03")
                .message("Internal Server Error.")
                .user(null)
                .build(),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

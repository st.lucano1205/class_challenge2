package com.group2.challenge2.service.imp;

import com.google.gson.*;
import com.group2.challenge2.model.entity.User;
import com.group2.challenge2.service.IUserService;
import org.springframework.stereotype.Service;

import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class UserImpService implements IUserService {

    @Override
    public User findByIdentityInfo(String typeID, String numberID) {
        String fileRoute = "src/main/resources";
        String fileName = "users.json";

        Gson gson = new Gson();


        if (validateInfo(typeID, numberID)){

            User userFound = null;

            try {
                Path path = Paths.get(fileRoute + "/" + fileName);

                Reader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8);
                JsonParser jsonParser = new JsonParser();
                JsonElement tree = jsonParser.parse(reader);

                JsonArray jsonArray = tree.getAsJsonArray();

                for (JsonElement element : jsonArray) {
                    if (element.isJsonObject()) {
                        JsonObject jsonUser = element.getAsJsonObject();
                        userFound = gson.fromJson(jsonUser, User.class);

                        if (validateUser(userFound, typeID, numberID))
                            return userFound;

                    }
                }
            } catch (Exception ex) {
                return null;
            }
        }
        return null;
    }

    public boolean validateInfo(String type, String number) {
        try {
            Integer.parseInt(number);
            return type.equalsIgnoreCase("C") || type.equalsIgnoreCase("P");
        } catch (Exception ex) {
            return false;
        }
    }

    private boolean validateUser(User user, String type, String number) {
        return user.getTypeID().equalsIgnoreCase(type) && user.getNumberID().equals(number);
    }
}

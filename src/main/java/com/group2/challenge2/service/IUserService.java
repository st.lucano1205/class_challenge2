package com.group2.challenge2.service;

import com.group2.challenge2.model.entity.User;

public interface IUserService {
    User findByIdentityInfo(String typeDocument, String numberDocument);
}
